const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const env = require('process').env;
const HTMLWebpackPlugin = require('html-webpack-plugin');

const production = env.NODE_ENV === 'production';
let plugins = [
  new HTMLWebpackPlugin({
    title: 'Share Calc'
  })
]

if (production) {
  plugins.push(
    new MiniCssExtractPlugin({
      filename: production ? 'styles.[hash].css' : 'styles.css'
    })
  )
}

const babelOptions = {
  presets: [
    [
      '@babel/preset-env',
      {
        // leave imports as they are
        modules: false,
        useBuiltIns: 'entry',
        targets: {
          browsers: [
            'last 2 versions'
          ]
        }
      }
    ]
  ],
  plugins: [
    '@babel/plugin-syntax-dynamic-import'
  ]
};

module.exports = {
  entry: './src/javascript/app.tsx',
  devtool: production ? 'source-map' : 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.(jpe?g|svg|woff2?)$/,
        use: 'file-loader'
      },
      {
        test: /\.tsx?$/,
        use: [
          { loader: 'babel-loader', options: babelOptions },
          { loader: 'ts-loader' }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: process.env.NODE_ENV !== 'production'
                    ? 'style-loader'
                    : MiniCssExtractPlugin.loader
          },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
        ]
      }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  output: {
    filename: production ? '[name].bundle.[hash].js' : '[name].bundle.js',
    chunkFilename: production ? '[name].bundle.[chunkhash].js' : '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: process.env.PUBLIC_PATH || '/'
  },
  plugins: plugins,
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    historyApiFallback: true
  }
};
