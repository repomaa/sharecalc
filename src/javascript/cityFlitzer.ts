const BASE_ADDRESS = 'https://web.book-n-drive.de/api'

export enum VehicleType {
  CityFlitzer = '506'
}

interface Vehicle {
  additionalInfo: { chargeLevel: number }
  bookable: boolean
  brand: string
  currentAddress: string
  licencePlate: string
  links: { booking: string }
  position: H.geo.Point
  station?: string
  vehicleType: VehicleType
  distance: number
}

export async function fetchVehicles(searchPosition: H.geo.Point, bounds: H.geo.Rect): Promise<Array<Vehicle>> {
  const [{ lat: lat0, lng: lon0 }, { lat: lat1, lng: lon1 }] = [
    bounds.getTopLeft(), bounds.getBottomRight()
  ]

  while (true) {
    const result = await fetch(`${BASE_ADDRESS}/vehicles?bounding_box=${lon0},${lat0},${lon1},${lat1}`)
    if (!result.ok) continue

    return (
      await result.json()
    ).map(({ position: { latitude: lat, longitude: lon }, ...rest }: any) => {
      const position = new H.geo.Point(lat, lon)
      const distance = position.distance(searchPosition)
      return { ...rest, position, distance }
    }).sort(({ distance: a }: Vehicle, { distance: b }: Vehicle) => a - b)
  }
}
