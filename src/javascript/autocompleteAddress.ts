import { appID, appCode } from './heremaps'

const baseURL = 'https://autocomplete.geocoder.api.here.com/6.2/suggest.json'

export default (query: string) => {
  return fetch(`${baseURL}?app_id=${appID}&app_code=${appCode}&query=${query}`)
    .then(result => result.json())
    .then(({ suggestions }) => suggestions.map(({ label }: { label: string }) => label))
}
