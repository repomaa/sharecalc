export enum Provider {
  BookNDrive = "Book'n'Drive",
  ShareNow = 'ShareNow'
}

export enum CarType {
  CityFlitzer = 'CityFlitzer',
  XS = 'XS',
  S = 'S',
  M = 'M',
  L = 'L',
}

export interface Vehicle {
  provider: Provider
  name: string
  type: CarType
  licensePlate: string
  position: H.geo.Point
  seats: number
  gasLevel: number
}
