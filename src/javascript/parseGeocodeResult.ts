export default ({
  Response: {
    View: [{
      Result: [{
        Location: {
          Address: { Label: address },
          DisplayPosition: { Latitude: latitude, Longitude: longitude }
        }
      }]
    }]
  }
}: any) => (
  { address, position: new H.geo.Point(latitude, longitude) }
)
