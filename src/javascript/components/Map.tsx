import { h, render, Component } from 'preact'
import { initHere } from '../heremaps'

export interface Props {
  platform: H.service.Platform
}

export default class Map extends Component<Props, {}> {
  element?: HTMLDivElement
  map?: H.Map
  vehicleMarkers: H.map.Marker[] = []

  render() {
    return <div ref={el => this.element = el} />;
  }

  componentDidMount() {
    const { element, props: { platform } } = this
    const defaultLayers = platform.createDefaultLayers()
    if (!element) throw new Error("Map DOM element was not set")
    const map = new H.Map(element, defaultLayers.normal.map)
    const mapEvents = new H.mapevents.MapEvents(map)
    const behavior = new H.mapevents.Behavior(mapEvents)
    const ui = H.ui.UI.createDefault(map, defaultLayers)
  }
}
