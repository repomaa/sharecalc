import { h } from 'preact'
import { State } from '../store'
import { connect } from 'redux-zero/preact'
import TripForm from './TripForm'
import VehicleList from './VehicleList'
import moment from 'moment'
import Prices from '../prices'
import { Provider, CarType } from '../vehicle'

interface Props {
  platform: H.service.Platform
  tripLength: number
  tripDuration: number
}

function sortedPrices(time: number, distance: number) {
  const prices = [] as { provider: string, carType: string, price: number, package?: string }[]

  Object.entries(Prices).forEach(([provider, providerPrices]) => {
    Object.entries(providerPrices).forEach(([carType, priceFunction]) => {
      prices.push({ provider, carType, ...priceFunction(time, distance) })
    })
  })

  return prices.sort(({ price: a }, { price: b }) => a - b)
}

const TripPlanner = ({ platform, tripLength, tripDuration }: Props) => (
  <div class="columns">
    <div class="column col-md-12 col-3">
      <h3>Summary</h3>
      <p>
        Total Distance: {Math.round(tripLength * 100 / 1000) / 100} km<br/>
        Total Duration: {moment.duration(tripDuration, 'seconds').humanize()}<br/>
      </p>
    </div>
    <div class="column col-sm-12 col-9">
      <TripForm {...{ platform }}/>

      <h3>Prices</h3>
      <table class="table">
        <thead>
          <tr>
            <th>Provider</th>
            <th>Car Type</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sortedPrices(tripDuration, tripLength).map(({ provider, carType, price, package: pkg }) => (
            <tr>
              <td>{provider}</td>
              <td>{carType}{pkg && ` (${pkg} Package)`}</td>
              <td>{Math.round(price * 100) / 100} €</td>
            </tr>
          ))}
        </tbody>
      </table>

      <VehicleList />
    </div>
  </div>
)

const mapToProps = ({ waypoints }: State) => {
  if (waypoints.length < 2) return { tripLength: 0, tripDuration: 0 }

  const tripLength = waypoints.reduce((tripLength, { distance }) => (
    tripLength + distance
  ), 0)
  const { departureTime: firstDepartureTime } = waypoints[0]
  const {
    departureTime: lastDepartureTime,
    duration: lastDuration
  } = waypoints[waypoints.length - 1]
  const tripDuration = lastDepartureTime.diff(firstDepartureTime, 'seconds')
  return { tripLength, tripDuration }
}

export default connect(
  mapToProps
)(TripPlanner)
