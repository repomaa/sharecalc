import { h } from 'preact'
import { default as moment, Moment } from 'moment'

interface Props {
  address: string
  index: number
  minTime: Moment
  departureTime: Moment
  changeDepartureTime: (index: number, time: Moment) => any
  hasNext: boolean
}

function handleDateChange(index: number, departureTime: Moment, changeDepartureTime: (index: number, time: Moment) => any) {
  return ({ target }: Event) => {
    const { value } = target as HTMLInputElement
    changeDepartureTime(index, moment(`${value} ${departureTime.format('HH:MM:SS')}`))
  }
}

function handleTimeChange(index: number, departureTime: Moment, changeDepartureTime: (index: number, time: Moment) => any) {
  return ({ target }: Event) => {
    const { value } = target as HTMLInputElement
    changeDepartureTime(index, moment(`${departureTime.format('YYYY-MM-DD')} ${value}`))
  }
}

interface FormProps {
  index: number
  minTime: Moment
  departureTime: Moment
  changeDepartureTime: (index: number, time: Moment) => any
}

const DepartureForm = ({ index, minTime, departureTime, changeDepartureTime }: FormProps) => {
  const minDate = minTime.clone().startOf('day')
  const departureDate = departureTime.clone().startOf('day')

  const minTimeToday = moment(`${moment().format('YYYY-MM-DD')} ${minTime.format('HH:mm')}`)
  const timeToday = moment(`${moment().format('YYYY-MM-DD')} ${departureTime.format('HH:mm')}`)

  return (
    <form>
      <label class="form-label form-inline" for={`departure-date-${index}`}>Departure date</label>
      <input
        type="date"
        class={`form-input form-inline${departureDate.isBefore(minDate) && ' has-error'}`}
        id={`departure-date-${index}`}
        onInput={handleDateChange(index, departureTime, changeDepartureTime)}
        min={minTime.format('YYYY-MM-DD')}
        value={departureTime.format('YYYY-MM-DD')}
      />
      {departureDate.isBefore(minDate) &&
        <p class="form-input-hint">Cannot be earlier than {minDate.format('YYYY-MM-DD')}</p>
      }

      <label class="form-label form-inline" for={`departure-time-${index}`}>Departure time</label>
      <input
        type="time"
        class={`form-input form-inline${!departureDate.isAfter(minDate) && timeToday.isBefore(minTimeToday) && ' has-error'}`}
        id={`departure-time-${index}`}
        onInput={handleTimeChange(index, departureTime, changeDepartureTime)}
        min={departureDate.isAfter(minDate) ? '00:00' : minTime.format('HH:mm')}
        value={departureTime.format('HH:mm')}
      />
      {!departureDate.isAfter(minDate) && timeToday.isBefore(minTimeToday) &&
        <p class="form-input-hint">Cannot be earlier than {minTime.format('HH:mm')}</p>
      }
    </form>
  )
}

export default ({ address, index, minTime, departureTime, changeDepartureTime, hasNext }: Props) => (
  <li>
    {address}
    {hasNext && <DepartureForm {...{ index, minTime, departureTime, changeDepartureTime }} />}
  </li>
)

