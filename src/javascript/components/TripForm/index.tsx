import { h } from 'preact'
import { State, Waypoint } from '../../store'
import { connect } from 'redux-zero/preact'
import AutocompleteAddress from '../AutocompleteAddress'
import autocompleteAddress from '../../autocompleteAddress'
import parseGeocodeResult from '../../parseGeocodeResult'
import calculateRoute from '../../calculateRoute'
import WaypointItem from './WaypointItem'
import { default as moment, Moment } from 'moment'

const mapToProps = ({ start, waypoints, idleTime }: State) =>
  ({ start, waypoints, idleTime })

const actions = (_store: any) => ({
  setStart: (state: State, coords: H.geo.Point) => ({ ...state, start: coords }),
  addWaypoint: async (state: State, platform: H.service.Platform, query: string) => {
    const geocoder = platform.getGeocodingService()
    const result = await new Promise((resolve, reject) => {
      geocoder.geocode({ searchText: query }, resolve, reject)
    })
    const { address, position } = parseGeocodeResult(result)
    const { waypoints } = state

    const previous = state.waypoints[waypoints.length - 1]
    const router = platform.getRoutingService()
    const { routeLength: distance, routeDuration: duration } = previous
      ? await calculateRoute(router, [previous.position, position])
      : { routeLength: 0, routeDuration: 0 }

    const departureTime = previous
      ? previous.departureTime.clone().add(duration, 'seconds')
      : moment()

    const waypoint = { departureTime, distance, duration, address, position }
    return { ...state, waypoints: [...waypoints, waypoint] }
  },
  changeDepartureTime: (state: State, index: number, time: Moment) => {
    const { waypoints } = state
    const { departureTime: oldTime } = waypoints[index]
    const diff = time.diff(oldTime, 'seconds')

    return {
      ...state,
      waypoints: [
        ...waypoints.slice(0, index),
        { ...waypoints[index], departureTime: time },
        ...waypoints.slice(index + 1).map(({ departureTime: t, ...rest }) =>
          ({ ...rest, departureTime: t.clone().add(diff, 'seconds') })
        )
      ]
    }
  }
})


interface Props {
  platform: H.service.Platform
  start: H.geo.Point
  waypoints: Waypoint[]
  idleTime: number
  setStart: (coords: H.geo.Point) => State
  addWaypoint: (plattform: H.service.Platform, address: string) => State
  changeDepartureTime: (index: number, time: Moment) => State
}

let waypointInput: HTMLInputElement | undefined

const TripForm = ({ platform, waypoints, addWaypoint, changeDepartureTime }: Props) => (
  <section>
    <h3>Trip</h3>
    <form>
      <fieldset>
        <legend>Waypoints</legend>
        <ol>
          {waypoints.map((waypoint, index) => {
            const previous = waypoints[index - 1]
            const minTime = previous
              ? previous.departureTime.clone().add(previous.duration, 'seconds')
              : moment(0)
            const hasNext = index + 1 < waypoints.length
            return <WaypointItem {...waypoint} {...{ index, changeDepartureTime, minTime, hasNext }}/>
          })}
        </ol>
        <AutocompleteAddress
          onSelect={(address) => addWaypoint(platform, address)}
          {...{ platform }}
        />
      </fieldset>
    </form>
  </section>
)

export default connect(
  mapToProps,
  actions
)(TripForm)
