import { Component, h } from 'preact'
import uniqueId from 'lodash.uniqueid'
import Result from './Result'
import parseGeocodeResult from '../../parseGeocodeResult'

interface OnSelectFunction {
  (query: string): any
}

interface Props {
  platform: H.service.Platform
  onSelect: OnSelectFunction
}

interface State {
  query: string
  results: string[]
}

import { appID, appCode } from '../../heremaps'

export default class AutocompleteAddress extends Component<Props, State> {
  state: State = {
    query: '',
    results: []
  }
  inputElement?: HTMLInputElement
  id = uniqueId()
  baseURL = 'https://autocomplete.geocoder.api.here.com/6.2/suggest.json'

  onInput = async ({ target }: Event) => {
    const { value: query } = target as HTMLInputElement
    this.setQuery(query)
  }

  async getResults(query: string): Promise<string[]> {
    if (query === '') return []
    const { baseURL } = this
    const response = await fetch(`${baseURL}?app_id=${appID}&app_code=${appCode}&query=${query}`)
    const { suggestions } = await response.json()
    return suggestions.map(({ label }: { label: string }) => label)
  }

  setQuery = async (query: string) => {
    this.setState({ query })
    this.inputElement.focus()
    const results = await this.getResults(query)
    this.setState({ results })
  }

  setFromLocation = async () => {
    const { props: { platform } } = this
    const location = await new Promise<Position>((resolve, reject) =>
      navigator.geolocation.getCurrentPosition(resolve, reject)
    )
    const { coords: { latitude, longitude, accuracy } } = location

    const geocoder = platform.getGeocodingService()
    const result = await new Promise((resolve, reject) =>
      geocoder.reverseGeocode({
        mode: 'retrieveAddresses',
        prox: `${latitude},${longitude},${accuracy}`,
        maxresults: '1'
      }, resolve, reject)
    )
    const { address } = parseGeocodeResult(result)
    this.setQuery(address)
  }

  onConfirm = async () => {
    const { props: { onSelect }, state: { query } } = this
    onSelect(query)
    this.setQuery('')
  }

  render() {
    const {
      onInput, setQuery, onConfirm, setFromLocation, id,
      state: { query, results }
    } = this

    return (
      <div class="form-group">
        <label class="form-label" for={`autocomplete-${id}`}>Address</label>
        <div class="input-group">
          <input
            type="text"
            class="form-input"
            ref={el => this.inputElement = el}
            id={`autocomplete-${id}`}
            value={query}
            onInput={onInput}
          />
          <button type="button" class="btn input-group-btn" onClick={setFromLocation}>
            <i class="icon icon-location" />
          </button>
          <button
            type="button"
            class="btn btn-primary input-group-btn"
            onClick={onConfirm}
          >
            Add waypoint
          </button>
        </div>
        <ul class="autocomplete-results">
          {results.map(result => <Result label={result} onSelect={setQuery} />)}
        </ul>
      </div>
    )
  }
}
