import { h } from 'preact'

interface OnSelectFunction {
  (label: string): any
}

interface Props {
  label: string
  onSelect: OnSelectFunction
}

export default ({ label, onSelect }: Props) => (
  <li>
    <span class="autocomplete-result-label">{label}</span>
    <button
      type="button"
      class="btn btn-sm btn-action s-circle"
      onClick={() => onSelect(label)}
    >
      <i class="icon icon-arrow-up"></i>
    </button>
  </li>
)
