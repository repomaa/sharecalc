import { h } from 'preact'
import TripPlanner from './TripPlanner'
import { Provider } from 'redux-zero/preact'
import { store } from '../store'

export interface Props {
  platform: H.service.Platform
}

export default ({ platform }: Props) => (
  <Provider {...{ store }}>

    <div class="container">
      <TripPlanner {...{ platform }} />
    </div>
  </Provider>
)
