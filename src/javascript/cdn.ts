export default function cdn(url: string, module = false) {
  console.log(`Loading ${url}`)

  if (url.match('\.js$')) {
    const script = document.createElement('script')

    return new Promise(resolve => {
      script.src = url
      script.onload = resolve
      if (module) script.type = 'module'
      document.head.appendChild(script)
    })
  } else if(url.match('\.css$')) {
    const link = document.createElement('link')
    link.rel = 'stylesheet'

    return new Promise(resolve => {
      link.href = url
      link.onload = resolve
      document.head.appendChild(link)
    })
  } else {
    throw new Error(`Unsupported url: ${url}`)
  }
}
