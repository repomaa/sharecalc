import * as createStore from 'redux-zero'
import { Vehicle } from './vehicle'
import { Moment } from 'moment'

export interface Waypoint {
  address: string
  position: H.geo.Point
  departureTime: Moment
  distance: number
  duration: number
}

export interface State {
  start?: H.geo.Point
  idleTime: number
  waypoints: Waypoint[]
  vehicles: Vehicle[]
}

const initialState: State = {
  idleTime: 0,
  waypoints: [],
  vehicles: []
}

console.log(createStore)
export const store = (createStore as any)(initialState);
