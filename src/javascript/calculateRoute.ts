export default async (router: H.service.RoutingService, waypoints: H.geo.Point[]) => {
  const params = waypoints.reduce((acc, { lat, lng }, index) => ({
    ...acc, [`waypoint${index}`]: `geo!${lat},${lng}`
  }), { mode: 'fastest;car;traffic:disabled' })
  const result = await new Promise((resolve, reject) =>
    router.calculateRoute(params, resolve, reject)
  )

  const {
    response: {
      route: [{
        summary: {
          trafficTime: routeDuration, distance: routeLength
        }
      }]
    }
  } = result as any
  console.log(result)

  return { routeLength, routeDuration }
}
