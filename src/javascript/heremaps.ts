import cdn from './cdn'

export const appID = '0EXKH3s93lcrQ0CUoVt3'
export const appCode = 'msMsbqwrY8IdDOxc2TnMmQ'

export async function initHere() {
  console.log('Loading Here API')

  await cdn('https://js.api.here.com/v3/3.0/mapsjs-core.js')
  await Promise.all([
    cdn('https://js.api.here.com/v3/3.0/mapsjs-service.js'),
    cdn('https://js.api.here.com/v3/3.0/mapsjs-ui.js'),
    cdn('https://js.api.here.com/v3/3.0/mapsjs-ui.css'),
    cdn('https://js.api.here.com/v3/3.0/mapsjs-mapevents.js')
  ])

  return { platform: new H.service.Platform({ app_id: appID, app_code: appCode, useHTTPS: true }) }
}
