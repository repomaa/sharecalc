import 'babel-polyfill'
import '../styles/app.scss'

async function init() {
  const [{ h, render }, { default: App }, { platform }] = await Promise.all([
    import('preact'),
    import('./components/App'),
    import('./heremaps').then(({ initHere }) => initHere())
  ])

  const root: any = document.body.lastChild
  render(<App {...{ platform }} />, document.body, root)
}

init()
