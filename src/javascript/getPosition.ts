import { Position } from './interfaces'

export function getPosition() {
  return new Promise<Position>((resolve, reject) =>
    navigator.geolocation.getCurrentPosition(resolve, reject)
  )
}
