import { Provider, CarType } from './vehicle'

interface BookNDriveCalcOptions {
  kmPrice: number
  hourPrice: number
  dayPrice?: number
  reducedKmPrice?: number
}

function bookndriveCalc(
  time: number, distance: number, {
    kmPrice,
    hourPrice,
    dayPrice = hourPrice * 24,
    reducedKmPrice = kmPrice
  }: BookNDriveCalcOptions
) {
  const days = Math.floor(time / 60 / 60 / 24)
  const restTime = time - days * 24 * 60 * 60
  const totalTimePrice = days * dayPrice + Math.min(restTime / 60 / 60 * hourPrice, dayPrice)
  const km = Math.ceil(distance / 1000)
  const totalKmPrice = km > 50 ? 50 * kmPrice + (km - 50) * reducedKmPrice : km * kmPrice

  return { price: 2 + totalTimePrice + totalKmPrice }
}

interface ShareNowCalcOptions {
  minutePrice: number
  twoHourPackagePrice: number
  fourHourPackagePrice: number
  sixHourPackagePrice: number
  dayPackagePrice: number
  twoDayPackagePrice: number
}

function shareNowCalc(
  time: number, distance: number, {
    minutePrice,
    twoHourPackagePrice,
    fourHourPackagePrice,
    sixHourPackagePrice,
    dayPackagePrice,
    twoDayPackagePrice
  }: ShareNowCalcOptions
) {
  const prices = [] as { price: number, package?: string }[]
  const km = distance / 1000
  prices.push({
    price: time / 60 * minutePrice + (km > 200 ? Math.ceil(km - 200) * 0.19 : 0)
  })
  const hours = time / 60 / 60
  const packages = [
    { includedHours: 2, price: twoHourPackagePrice },
    { includedHours: 4, price: fourHourPackagePrice },
    { includedHours: 6, price: sixHourPackagePrice },
    { includedHours: 24, price: dayPackagePrice },
    { includedHours: 48, price: twoDayPackagePrice }
  ]

  packages.forEach(({ includedHours, price }) => {
    prices.push({
      package: `${includedHours}h`,
      price: price +
        (hours > includedHours ? Math.ceil((hours - includedHours) * 60) * minutePrice : 0) +
        Math.ceil(km) * 0.19
    })
  })

  return prices.sort(({ price: a }, { price: b }) => a - b)[0]
}

export default {
  [Provider.BookNDrive]: {
    [CarType.CityFlitzer](time: number, distance: number) {
      return bookndriveCalc(time, distance, {
        hourPrice: 1.50,
        dayPrice: 36.00,
        kmPrice: 0.25,
      })
    },
    [CarType.XS](time: number, distance: number) {
      return bookndriveCalc(time, distance, {
        hourPrice: 3.30,
        dayPrice: 33.00,
        kmPrice: 0.25,
        reducedKmPrice: 0.20,
      })
    },
    [CarType.S](time: number, distance: number) {
      return bookndriveCalc(time, distance, {
        hourPrice: 3.30,
        dayPrice: 44.00,
        kmPrice: 0.27,
        reducedKmPrice: 0.21,
      })
    },
    [CarType.M](time: number, distance: number) {
      return bookndriveCalc(time, distance, {
        hourPrice: 5.50,
        dayPrice: 55.00,
        kmPrice: 0.29,
        reducedKmPrice: 0.23,
      })
    },
    [CarType.L](time: number, distance: number) {
      return bookndriveCalc(time, distance, {
        hourPrice: 6.60,
        dayPrice: 66.00,
        kmPrice: 0.32,
        reducedKmPrice: 0.25,
      })
    }
  },
  [Provider.ShareNow]: {
    [CarType.XS](time: number, distance: number) {
      return shareNowCalc(time, distance, {
        minutePrice: 0.19,
        twoHourPackagePrice: 13.99,
        fourHourPackagePrice: 23.99,
        sixHourPackagePrice: 33.99,
        dayPackagePrice: 49.99,
        twoDayPackagePrice: 89.99,
      })
    },
    [CarType.S](time: number, distance: number) {
      return shareNowCalc(time, distance, {
        minutePrice: 0.28,
        twoHourPackagePrice: 15.99,
        fourHourPackagePrice: 26.99,
        sixHourPackagePrice: 37.99,
        dayPackagePrice: 49.99,
        twoDayPackagePrice: 99.99
      })
    },
    [CarType.M](time: number, distance: number) {
      return shareNowCalc(time, distance, {
        minutePrice: 0.31,
        twoHourPackagePrice: 17.99,
        fourHourPackagePrice: 29.99,
        sixHourPackagePrice: 39.99,
        dayPackagePrice: 69.99,
        twoDayPackagePrice: 119.99
      })
    },
    [CarType.L](time: number, distance: number) {
      return shareNowCalc(time, distance, {
        minutePrice: 0.34,
        twoHourPackagePrice: 19.99,
        fourHourPackagePrice: 34.99,
        sixHourPackagePrice: 44.99,
        dayPackagePrice: 79.99,
        twoDayPackagePrice: 139.99
      })
    }
  }
}
