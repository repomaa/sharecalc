export interface Coords {
  latitude: number
  longitude: number
}

export interface Position {
  coords: Coords
}

export interface BoundingBox {
  topLeft: Coords
  bottomRight: Coords
}
